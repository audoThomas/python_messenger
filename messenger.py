# -*- coding: utf-8 -*-

import Tkinter as tk
import socket
import sys
from PIL import Image, ImageTk
import time
import threading

class StdoutRedirect(object):
    def __init__(self, text_widget):
        self.text_widget = text_widget
 
    def write(self, s):
        self.text_widget.insert('end', s)
        self.text_widget.see('end')




class RootTkinter(tk.Tk):    # Création de la classe mère


    def __init__(self, *args, **kwargs):        # Création du constructeur __init__ de la classe
        tk.Tk.__init__(self, *args, **kwargs)
        self.iconbitmap('@asn_logo.xbm')        # Mise en place du logo ASN 
        self.title("ASN Messenger")             # Titre de la fenêtre


         # Création de la bar de menu en haut de la fenêtre
        self.menubar = tk.Menu(self)
        menu_server = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Server", menu=menu_server)
        menu_server.add_command(label="Create", command=lambda: self.show_frame("CreateFrame"))
        menu_server.add_command(label="Connect", command=lambda: self.show_frame("ConnectFrame"))
        menu_server.add_separator()
        menu_server.add_command(label="Close")
        menu_about = tk.Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="About", menu=menu_about)
        menu_about.add_command(label="Team", command=lambda: self.show_frame("Team"))
        menu_about.add_command(label="Project", command=lambda: self.show_frame("Project"))

        self.config(menu=self.menubar)


        # Regroupement des variables qui seront modifiées dans d'autres classes 
        self.get_HOST = tk.StringVar()
        self.get_PORT = tk.StringVar()
        self.get_NAME = tk.StringVar()
        self.TEXT_MESSAGE = tk.StringVar()


        
        # création de la boucle permettant de switcher entre les différents Frame
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (MainPage, CreateFrame, ConnectFrame, Team, Project):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("MainPage")

    def show_frame(self, page_name):      # Fonction qui redirige vers les Frames
        
        frame = self.frames[page_name]
        frame.tkraise()

    def config_socket(self, choice):       # Fonctionne qui permet de configurer le socket, le créer
    	if choice == 1:                    # quand on est le serveur, ou se connecter a un server deja créé
    		SET_HOST = self.get_HOST.get()	
    		SET_PORT = self.get_PORT.get()
    		SET_NAME = self.get_NAME.get()
    		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    		self.sock.bind((SET_HOST, int(SET_PORT)))
    		self.sock.listen(5)
    		self.client, adress = self.sock.accept()
    		time.sleep(1)
    		self.nom_client = self.client.recv(2048)

    		print "%s est Connecté(e)" % self.nom_client
    		self.client.send(SET_NAME)

    		self.thread = threading.Thread(target=self.boucle_msg_server)
    		self.thread.start()

    		
    	else:
    		SET_HOST = self.get_HOST.get()
    		SET_PORT = self.get_PORT.get()
    		SET_NAME = self.get_NAME.get()
    		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    		self.sock.connect((SET_HOST, int(SET_PORT)))
    		self.sock.send(SET_NAME)
    		self.nom_client = self.sock.recv(2048)

    		print "%s est Connecté(e)" % self.nom_client

    		self.thread = threading.Thread(target=self.boucle_msg_client)   # Permet de démarrer la boucle
    		self.thread.start()                                             # qui récupère les messages recus
                                                                            # en arrière plan

    def send_msg(self, choice):              # Fonction qui envoie les messages grace au boutton send
    	MESSAGE = self.TEXT_MESSAGE.get()    # les messages envoyés sont aussi imprimés par l'envoyeur
    	try:
    		self.client.send(MESSAGE)
    		print "</ Vous > ", MESSAGE
    	except AttributeError:
    		self.sock.send(MESSAGE)
    		print "</ Vous > ", MESSAGE




    def boucle_msg_client(self):                            # Ces deux fonctions créer la boucle que thread 
    	while True:                                         # va faire tourner. il y a une boucle pour le client
    			data = self.sock.recv(4096)                 # et une boucle pour le server (pas les mêmes nom de variables)
    			if data:
    				print "</",self.nom_client,"> ", data

    			else:
    				pass
    def boucle_msg_server(self):
    	while True:
    			data = self.client.recv(4096)
    			if data:
    				print "</",self.nom_client,"> ", data
    			else:
    				pass





class MainPage(tk.Frame):    # Création de la classe affichant une Frame avec la textbox 

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        frame = tk.Frame(self, width=200, height=300)
        frame.pack()

        var_message = tk.StringVar()                                   # Zone pour entrer les messages à envoyer
        self.entry_message = tk.Entry(self, textvariable=var_message)
        self.entry_message.pack(side="left")

        button_message = tk.Button(self, text="Send", command=lambda: self.get_msg())
        button_message.pack(side="right")                 				# Boutton qui renvoie a la fonction pour envoyer les messages

        self.textbox = tk.Text(frame, wrap='word')
        self.textbox.pack()

        sys.stdout = StdoutRedirect(self.textbox)                        # redirige le retour du terminal dans la textbox

    def get_msg(self):                                       # Fonction qui récupère les messages à envoyer
    	message = self.entry_message.get()
    	self.controller.TEXT_MESSAGE.set(message)
    	self.controller.send_msg(1)





class CreateFrame(tk.Frame):     # Classe permettant l'affichage de l'écran de création de socket

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        ost = tk.StringVar()                                          # création des zones pour rentrer 
        ort = tk.StringVar()                                          # l'adresse Ip, le port et le nom 
        ame = tk.StringVar()                                          # pour créer un server
        lblhost = tk.LabelFrame(self, text="Enter Server Ip :")
        lblport = tk.LabelFrame(self, text="Enter Server Port :")
        lblname = tk.LabelFrame(self, text="Enter your name :")
        self.ehost = tk.Entry(lblhost, textvariable=ost)
        self.eport = tk.Entry(lblport, textvariable=ort)
        self.ename = tk.Entry(lblname, textvariable=ame)
        self.ehost.pack()
        self.eport.pack()
        self.ename.pack()
        lblhost.pack()
        lblport.pack()
        lblname.pack()
        create_button = tk.Button(self, text="Connect", command=lambda: self.get_var())
        create_button.pack()
        

    def get_var(self):                   # Fonction qui récupère la valeurs des variables de l'hote, du port et du nom
    	HOST = self.ehost.get()
    	PORT = self.eport.get()
    	NAME = self.ename.get()
    	self.controller.get_HOST.set(HOST)
    	self.controller.get_PORT.set(PORT)
    	self.controller.get_NAME.set(NAME)
    	print "Identité : \n"
    	print "Adresse Ip du Server : %s\n" % HOST, "Port du Server : %s\n" % PORT, "Votre Nom : %s\n" % NAME

    	self.controller.config_socket(1)
    	self.controller.show_frame("MainPage")
    	





class ConnectFrame(tk.Frame):       # Classe permettant l'affichage de l'écran de connexion de socket

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        ost = tk.StringVar()                                        # Fonction qui récupère les variables
        ort = tk.StringVar()                                        # de l'hote, du port et du nom 
        ame = tk.StringVar()                                        # pour se connecter à un server éxistant
        lblhost = tk.LabelFrame(self, text="Enter Server Ip :")
        lblport = tk.LabelFrame(self, text="Enter Server Port :")
        lblname = tk.LabelFrame(self, text="Enter your name :")
        self.ehost = tk.Entry(lblhost, textvariable=ost)
        self.eport = tk.Entry(lblport, textvariable=ort)
        self.ename = tk.Entry(lblname, textvariable=ame)
        self.ehost.pack()
        self.eport.pack()
        self.ename.pack()
        lblhost.pack()
        lblport.pack()
        lblname.pack()
        create_button = tk.Button(self, text="Connect", command=lambda: self.get_var())
        create_button.pack()
        

    def get_var(self):                                  # Fonction qui récupère les variables hote, port, nom
    	HOST = self.ehost.get()                         # et qui affiche l'identité de l'utilisateur
    	PORT = self.eport.get()
    	NAME = self.ename.get()
    	self.controller.get_HOST.set(HOST)
    	self.controller.get_PORT.set(PORT)
    	self.controller.get_NAME.set(NAME)
    	print "Identité : \n"
    	print "Adresse Ip du Server : %s\n" % HOST, "Port du Server : %s\n" % PORT, "Votre Nom : %s\n" % NAME

    	self.controller.config_socket(2)
    	self.controller.show_frame("MainPage")


    	


class Team(tk.Frame):        # Classe présentant l'équipe de ce projet

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        lblframe = tk.LabelFrame(self, text="Notre equipe est composée de :")
        lblframe.pack()
        
        thomas_image = Image.open("thomas.jpg")                 # affiche les images de l'équipe dans un labelframe
        thomas_photo = ImageTk.PhotoImage(thomas_image)
        thomas_label = tk.Label(lblframe, image=thomas_photo)
        thomas_label.image = thomas_photo
        thomas_label.pack(side="left")

        luc_image = Image.open("luc.jpg")
        luc_photo = ImageTk.PhotoImage(luc_image)
        luc_label = tk.Label(lblframe, image=luc_photo)
        luc_label.image = luc_photo
        luc_label.pack(side="right")
                                                               # Présente l'équipe avec des petis textes
        thomas_texte = tk.Label(lblframe, text="Thomas Audo, chargé du developpement Web.\nPasionné d'histoire, d'internet et aussi d\'informatique en général.")
        thomas_texte.pack(side="bottom")

        luc_texte = tk.Label(lblframe, text="Luc Beaufils, chargé du developpement Python.\nS'intérrèse au système libre Linux et également aux sports.")
        luc_texte.pack()

        back_button = tk.Button(self, text="Back", command=lambda: controller.show_frame("MainPage"))
        back_button.pack()            # Bouton retour a la page de départ

class Project(tk.Frame):     # Classe présentant le projet

	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)
		self.controller = controller
		lblframe = tk.LabelFrame(self, text="Presentation de notre projet :")
		lblframe.pack()
		lbl = tk.Label(lblframe, text="ASN est un système de messagerie\ndéveloppé en Python par Luc Beaufils et Thomas Audo dans le cadre d'un projet d'ISN.\nPour en savoir plus : thomaslucisn.000webhost.com ")
		lbl.pack()


if __name__ == "__main__":
    app = RootTkinter()
    app.mainloop()

